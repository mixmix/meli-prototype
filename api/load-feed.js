const CRUT = require('ssb-crut')
const wikiSpec = require('../spec/wiki')

module.exports = (peer) => (req, res, next) => {
  if (!req.query.id) return res.send('Error: please send ?id')

  const feedpurpose = 'meli/' + req.query.id

  peer.metafeeds.findOrCreate((err, rootFeed) => {
    if (err) {
      console.log(err, '\n')
      return res.send('Error: could not load feed')
    }

    peer.metafeeds.findOrCreate(
      rootFeed,
      (feedDetails) => feedDetails.feedpurpose === feedpurpose,
      {
        feedpurpose,
        feedformat: 'classic'
      },
      (err, feedDetails) => {
        if (err) {
          console.log(err, '\n')
          return res.send('Error: could not load feed')
        }

        console.log(feedDetails)
        req.feed = {
          id: feedDetails.keys.id,
          wiki: new CRUT(peer, {
            ...wikiSpec,
            publish: (content, cb) => {
              console.log({ content })
              peer.db.create({ content, keys: feedDetails.keys }, cb)
            }
          })
        }
        next()
      }
    )
  })
}
