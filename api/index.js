const express = require('express')
const bodyParser = require('body-parser')
const os = require('os')
const loadFeed = require('./load-feed')

module.exports = (peer) => {
  const app = express()
  app.use(bodyParser.json())

  const handleRes = (res) => (err, data) => {
    if (err) return res.send('Error')
    else res.send(tidy(data))
  }

  app.get('/wiki', (req, res) => {
    peer.wiki.list({}, handleRes(res))
  })

  app.post('/wiki/:id', (req, res) => {
    peer.wiki.read(req.params.id, handleRes(res))
  })

  /* Methods which require user ?id */
  app.use(loadFeed(peer))

  app.post('/wiki', (req, res) => {
    req.feed.wiki.create(req.body, handleRes(res))
  })

  app.listen(3000)
  console.log(`listening on:
    http://localhost:3000
    http://${findLocalAddress()}:3000
  `)
}

function findLocalAddress () {
  return Object.values(os.networkInterfaces()).flatMap(a => a)
    .find(data => (
      data.family === 'IPv4' &&
      !data.address.startsWith('127')
    ))
    ?.address
}

function tidy (data) {
  if (Array.isArray(data)) return data.map(datum => ({ ...datum, states: undefined }))
  if (typeof data === 'object') {
    data.states = undefined
    return data
  }

  return data
}
