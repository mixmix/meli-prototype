const Overwrite = require('@tangle/overwrite')

module.exports = {
  type: 'wiki/meli',
  props: {
    title: Overwrite({ type: 'string' }),
    description: Overwrite({ type: 'string' })
  }
}
