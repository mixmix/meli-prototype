const Stack = require('secret-stack')
const caps = require('ssb-caps')
const path = require('path')
const keys = require('ssb-keys')

const CRUT = require('ssb-crut')
const wikiSpec = require('../spec/wiki')

module.exports = (config) => {
  const Peer = Stack({ caps })
    .use(require('ssb-db2'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-bendy-butt'))
    .use(require('ssb-meta-feeds'))

  const peer = Peer({
    path: __dirname,
    keys: keys.loadOrCreateSync(path.join(__dirname, 'secret')),
    ...config
  })

  peer.wiki = new CRUT(peer, wikiSpec)

  console.log(`peer id:\n  ${peer.id}`)

  return peer
}
